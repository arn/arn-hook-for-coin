#!/usr/bin/python3
import json
import csv
import sys
from subprocess import Popen, PIPE
from email.mime.text import MIMEText
from pathlib import Path
import time
import datetime
import os
import re
from OpenSSL import crypto

# On ARN infra, for security reasons we force the command executed by the SSH
# user, however we want to know the original command sent.
command = os.environ['SSH_ORIGINAL_COMMAND']

# Display in COIN the state of the service
if command == 'states':
    answer = []

    states = {}
    # Get openvpn account info from our openvpn server
    for user in os.listdir('/etc/openvpn/users'):
        if os.path.isfile('/etc/openvpn/users/' + user):

            ipv4 = [line.replace('IP4="','').replace('"\n', '')
                    for line in open('/etc/openvpn/users/' + user)
                    if re.match(r'^IP4="(.+)"$', line)][0]
            crypto_link = [line.replace('LINK="','').replace('"\n', '')
                    for line in open('/etc/openvpn/users/' + user)
                    if re.match(r'^LINK="(.+)"$', line)]
            crypto_link = crypto_link[0] if len(crypto_link) > 0 else ''
            try:
                cert = crypto.load_certificate(crypto.FILETYPE_PEM, open('/etc/openvpn/easy-rsa/keys/' + user + '.crt').read())
                expire = datetime.datetime.strptime(cert.get_notAfter().decode('utf-8'), "%Y%m%d%H%M%SZ")
            except FileNotFoundError:
                expire = None
            states[ipv4] = {'expire': expire, 'crypto_link': crypto_link}


    # We received on stdin the list of users we should have on the openvpn server
    for row in csv.DictReader(iter(sys.stdin.readline, ''), delimiter=';'):
        ipv4 = row['ipv4'].replace('/32', '')
        if ipv4 in states.keys():
            expire_date = states[ipv4]['expire']
            if expire_date:
                nb_days = expire_date - datetime.datetime.now()
                if nb_days > datetime.timedelta(15):
                    status_color = 'green'
                elif nb_days >= datetime.timedelta(1):
                    status_color = 'orange'
                elif nb_days < datetime.timedelta(1):
                    status_color = 'red'
                status = 'Expire le ' + expire_date.strftime("%Y-%m-%d")
            else:
                status = 'error'
                status_color = 'red'
            answer.append({
                'id': row['id'],
                'provisioned': 'yes',
                'status': status,
                'status_color': status_color,
                'crypto_link': states[ipv4]['crypto_link']
            })
            del states[ipv4]
        else:
            answer.append({
                'id': row['id'],
                'provisioned': 'no',
                'status': 'N/A',
                'status_color': 'red'
            })

    # Return the state list via json (we configured "parse_output": "json" in coin settings file)
    print(json.dumps(answer))

    # Send a mail if some VPN are provisionned but not bind to an openvpn
    # configuration in COIN
    if len(states.keys()) > 0:
        from pathlib import Path
        lastmail = Path('lastmail')
        if not lastmail.exists() or (time.time() - lastmail.stat().st_mtime) / 60 / 60 / 24 > 1:
            msg = MIMEText("Il y a des comptes VPN qui ne sont pas répercutés dans COIN: {ips}".format(ips=str(states.keys())))
            msg['Subject'] = 'Incohérence COIN/VPN'
            msg['From'] = 'root@arn-fai.net'
            msg['To'] = 'infra-support@arn-fai.net'

            p = Popen(["/usr/sbin/sendmail", "-t", "-oi"], stdin=PIPE)
            p.communicate(msg.as_bytes())
            lastmail.touch()

# Provision the openvpn account
elif command == 'create':
    # Chaque ligne du CSV reçu sur stdin décris un nouveau compte à créer
    for row in csv.DictReader(iter(sys.stdin.readline, ''), delimiter=';'):

        # Check entry data
        login = row['login']
        ipv4 = row['ipv4'].replace('/32','')
        ipv6 = row['ipv6'].replace('/128','')
        prefix = row['prefix']
        regex_ipv6 = r'(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))'

        assert re.match(r'[a-z0-9-]{3,15}', login), "Login incorrect : {login}".format(login=login)
        assert re.match(r'(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)', ipv4), "IPv4 incorrecte : {ipv4}".format(ipv4=ipv4)
        assert re.match(regex_ipv6, ipv6), "IPv6 incorrecte : {ipv6}".format(ipv6=ipv6)
        assert re.match(regex_ipv6 + r'/56', prefix), "Préfix incorrecte : {prefix}".format(prefix=prefix)

        # Run the script to generate the VPN
        p = Popen(["sudo", "/usr/local/sbin/manage-vpn", 'create', '-l', login, '-4', ipv4, '-6', ipv6, '-p', prefix], stdout=PIPE)
        out, err = p.communicate()
    print("{}")

# Remove or renew the openvpn
elif command == 'remove' or command == 'renew':
    for row in csv.DictReader(iter(sys.stdin.readline, ''), delimiter=';'):
        ipv4 = row['ipv4'].replace('/32','')
        assert re.match(r'(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)', ipv4), "IPv4 incorrecte : {ipv4}".format(ipv4=ipv4)

        p = Popen(["sudo", "/usr/local/sbin/manage-vpn", command, '-4', ipv4], stdout=PIPE)
        out, err = p.communicate()
    print("{}")

# Unknown command
else:
    print('Unknown command: ' + command)
