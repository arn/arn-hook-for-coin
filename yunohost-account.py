#!/usr/bin/python3
import json
import csv
import sys
from subprocess import Popen, PIPE
from email.mime.text import MIMEText
from pathlib import Path
import time
import random
import string
import os
import re


NON_MEMBERS_ACCOUNT = ['admin2', 'inscriptions', 'mobilizon_notifs', 'mautrix_admin', 'forum']
RESERVED_GROUPS = ['visitors', 'all_users', 'adminsys']
INDIVIDUAL_OFFER_REGEX = r'SN-5G|SN-5G-sup'
GROUP_OFFER_REGEX = r'SN-ASSO-15G'
MEMBER_GROUP_OFFER_REGEX = r'SN-ASSO-1G'
PASSWORD_LENGTH = 20

# On ARN infra, for security reasons we force the command executed by the SSH
# user, however we want to know the original command sent.
command = os.environ['SSH_ORIGINAL_COMMAND']

# Display in COIN the state of the service
if command == 'states':
    answer = []

    # Get users info from our yunohost
    p = Popen(["sudo", "yunohost", "user", "list", "--output-as", "json"], stdout=PIPE)
    out, err = p.communicate()
    states = json.loads(out.decode('utf-8'))
    states = states['users']

    # We received on stdin the list of users we should have on the yunohost
    for row in csv.DictReader(iter(sys.stdin.readline, ''), delimiter=';'):
        # If the login is listed by yunohost, we consider the service as working
        # and provisionned
        if row['login'] in states.keys():
            answer.append({
                'id': row['id'],
                'provisioned': 'yes',
                'status': 'actif',
                'status_color': 'green'
            })

            # In this case we delete it from the list
            del states[row['login']]
        else:
            answer.append({
                'id': row['id'],
                'provisioned': 'no',
                'status': '',
                'status_color': 'red'
            })

            # We delete all non members normal account
            if row['login'] in NON_MEMBERS_ACCOUNT:
                del states[row['login']]

    # Return the state list via json (we configured "parse_output": "json" in coin settings file)
    print(json.dumps(answer))

    # At this steps , we have only anormal account in yunohost but unlisted in COIN
    # We want to receive an email to manage this properly by an admin
    if len(states.keys()) > 0:
        from pathlib import Path
        lastmail = Path('lastmail')
        if not lastmail.exists() or (time.time() - lastmail.stat().st_mtime) / 60 / 60 / 24 > 1:
            msg = MIMEText("Il y a des comptes sans-nuage qui ne sont pas répercutés dans COIN: {usernames}".format(usernames=str(states.keys())))
            msg['Subject'] = 'Incohérence COIN/Sans-nuage'
            msg['From'] = 'root@sans-nuage.fr'
            msg['To'] = 'sans-nuage-support@arn-fai.net'

            p = Popen(["/usr/sbin/sendmail", "-t", "-oi"], stdin=PIPE)
            p.communicate(msg.as_bytes())
            lastmail.touch()


# Provision the yunohost account
elif command == 'create':

    # Chaque ligne du CSV reçu sur stdin décris un nouveau compte à créer
    for row in csv.DictReader(iter(sys.stdin.readline, ''), delimiter=';'):

        # The login wanted for the external account could be distinct from the
        # username used in COIN. However for groups creation we use the COIN
        # username.
        login = row['login']
        mail = row['mail']
        firstname = row['firstname']
        username = row['username'].lower()

        # A yunohost group can't be named identically with an other user
        if username == login and re.match(GROUP_OFFER_REGEX, row['offer_ref']):
            login += "_admin"

        # Check the username is not reserved
        if username in RESERVED_GROUPS:
            print('Invalid groupname')
            os.exit(1)

        # Check future yunohost login, firstname and lastname
        lastname = row['lastname']
        regex_name = r'[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð \'-]+'
        assert re.match(r'[a-z0-9_]{1,25}', login), "Login incorrect : {login}".format(login=login)
        assert re.match(regex_name, login), "Firstname : {firstname}".format(firstname=firstname)
        assert re.match(regex_name, login), "Lastname : {lastname}".format(lastname=lastname)

        # Generate a random password for the user
        letters = string.ascii_lowercase
        password = ''.join(random.choice(letters) for i in range(PASSWORD_LENGTH))

        # Create individual yunohost account
        if re.match(INDIVIDUAL_OFFER_REGEX, row['offer_ref']):
            p = Popen(["sudo", "/usr/bin/yunohost", 'user', 'create', '-f', firstname, '-l', lastname, '-m', login + '@sans-nuage.fr', '-q', '2G', '-p', password, login], stdout=PIPE)
            out, err = p.communicate()
            p = Popen(["sudo", "-u", 'nextcloud', 'php', '/var/www/nextcloud/occ', 'user:setting', login, 'files', 'quota', '3GB'], stdout=PIPE)
            out, err = p.communicate()

        # Create group yunohost account
        elif re.match(GROUP_OFFER_REGEX, row['offer_ref']):
            p = Popen(["sudo", "/usr/bin/yunohost", 'user', 'create', '-f', firstname, '-l', lastname, '-m', login + '@sans-nuage.fr', '-q', '5G', '-p', password, login], stdout=PIPE)
            out, err = p.communicate()
            p = Popen(["sudo", "-u", 'nextcloud', 'php', '/var/www/nextcloud/occ', 'user:setting', login, 'files', 'quota', '10GB'], stdout=PIPE)
            out, err = p.communicate()
            p = Popen(["sudo", "/usr/bin/yunohost", 'user', 'group', 'create', username], stdout=PIPE)
            out, err = p.communicate()
            p = Popen(["sudo", "/usr/bin/yunohost", 'user', 'group', 'add', username, login], stdout=PIPE)
            out, err = p.communicate()

        # Create a member of a group
        elif re.match(MEMBER_GROUP_OFFER_REGEX, row['offer_ref']):
            p = Popen(["sudo", "/usr/bin/yunohost", 'user', 'create', '-f', firstname, '-l', lastname, '-m', login + '@sans-nuage.fr', '-q', '1G', '-p', password, login], stdout=PIPE)
            out, err = p.communicate()
            p = Popen(["sudo", "/usr/bin/yunohost", 'user', 'group', 'add', username, login], stdout=PIPE)
            out, err = p.communicate()
        else:
            print('Unknown offer reference')
            os.exit(1)

        # Send an email to our new user with the info
        msg = MIMEText("Bonjour {firstname} {lastname},\n\nVotre compte sans-nuage.fr vient d'être créé :)\n\n Pour se connecter:\nIdentifiant: {login}\nMot de passe à changer: {password}\nInterface de connexion: https://sans-nuage.fr/fr/user.php\n\nPlus d'info et d'aide se trouve dans l'espace membre : https://adherents.arn-fai.net\n\nUne question ? -> support@sans-nuage.fr\n\nBonne journée,\nCeci est un message automatique".format(firstname=firstname, lastname=lastname, password=password, login=login))
        msg['Subject'] = 'Votre compte sans-nuage.fr est prêt :)'

        msg['From'] = 'support@sans-nuage.fr'
        msg['To'] = mail

        p = Popen(["/usr/sbin/sendmail", "-t", "-oi"], stdin=PIPE)
        p.communicate(msg.as_bytes())

    # Return an empty json
    print("{}")


# Remove the yunohost account
elif command == 'remove':
    for row in csv.DictReader(iter(sys.stdin.readline, ''), delimiter=';'):
        login = row['login']
        assert re.match(r'[a-z0-9-]{3,15}', login), "Login incorrect : {login}".format(login=login)

        p = Popen(["sudo", "/usr/bin/yunohost", 'user', 'delete', '--purge', login], stdout=PIPE)
        out, err = p.communicate()
    print("{}")


# Unknown command management
else:
    print('Unknown command: ' + command)
