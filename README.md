# arn-hook-for-coin

Hook for automation vps/vpn/yunohost creation into COIN.

## Installation

* Add something like this in the settings_local.py of COIN
```
HOOKS = {
    "external_account": {
        "FETCH_ALL_STATES": {"type": "bash",
                             "remote": "coin@my-chatons.com",
                             "command": "states",
                             "stdin": u"{conf_list_csv}",
                             "parse_output": "json"},
        "PROVISION": {"type": "bash",
                             "remote": "coin@my-chatons.com",
                             "command": "create",
                             "stdin": u"id;offer_name;offer_ref;username;login;firstname;lastname;mail\n{id};{offer_name};{offer_ref};{username};{login};{firstname};{lastname};{mail}",
                             "parse_output": "json"},
    },
    "vpn": {
        "FETCH_ALL_STATES": {"type": "bash",
                             "remote": "coin@vpn.my-fai.com",
                             "command": "states",
                             "stdin": u"{conf_list_csv}",
                             "parse_output": "json"},
        "PROVISION": {"type": "bash",
                             "remote": "coin@vpn.my-fai.com",
                             "command": "create",
                             "stdin": u"id;ipv4;ipv6;prefix;offer_name;offer_ref;username;login\n{id};{ipv4};{ipv6};{prefix};{offer_name};{offer_ref};{username};{login}",
                             "parse_output": "json"},
        "DEPROVISION": {"type": "bash",
                             "remote": "coin@vpn.my-fai.com",
                             "command": "remove",
                             "stdin": u"id;ipv4\n{id};{ipv4}",
                             "parse_output": "json"}
    },
    "vps": {
        "FETCH_ALL_STATES": {"type": "bash",
                             "remote": "coin@cluster.my-fai.com",
                             "command": "states",
                             "stdin": u"{conf_list_csv}",
                             "parse_output": "json"}
    }
}
```

* On machine cluster.my-fai.com, vpn.my_fai.com, my_chatons.com
```
mkdir -p /opt/coin
adduser -d /opt/coin coin
mkdir -p /opt/coin/.ssh
# Make sur eto force the command COIN
echo 'command="sudo --preserve-env /opt/coin/SCRIPT.py",no-pty,no-agent-forwarding,no-port-forwarding,no-X11-forwarding,no-user-rc ssh-ed25519 AAAA[..]' > /home/coin/.ssh/authorized_keys
chmod 700 /home/coin/.ssh
chmod 600 /home/coin/.ssh/authorized_keys
```
